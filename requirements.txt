Django==3.1.2
python-decouple==3.3
requests==2.24.0
django-ipware==3.0.2
virtualenv==20.0.35
django-cors-headers==3.6.0

