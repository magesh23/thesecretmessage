from django.urls import path
from message import views
from message import profile
from message import message
from message import errorhandler

from django.views.generic.base import TemplateView

from django.contrib.sitemaps.views import sitemap
from message.sitemaps import StaticViewSitemap
sitemaps = {
    'static': StaticViewSitemap
}

urlpatterns = [
    path('', views.home, name="Home"),
    path('link/<str:userid>/', views.messagePage, name="MessagePage"),

    path('api/create/user/', profile.createUser, name="CreateUser"),

    path('api/create/message/', message.createMessage, name="CreateMessage"),
    path('api/delete/message/', message.deleteMessage, name="DeleteMessage"),
    path('api/get/message/', message.getMessage, name="GetMessage"),

    # meta pages
    path('terms/', views.termsCondition, name="TermsCondition"),
    path('privacy/', views.privacyPolicy, name="PrivacyPolicy"),
    path('faq/', views.faq, name="Faq"),
    path('about/', views.aboutUs, name="AboutUs"),
    path('contact/', views.contactUs, name="ContactUs"),

    # error handling url
    path('404', errorhandler.handler404, name="404Page"),
    path('500', errorhandler.handler500, name="500Page"),
    path('403', errorhandler.handler403, name="403Page"),
    path('400', errorhandler.handler400, name="400Page"),

    # sitemap
    path('sitemap.xml', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap'),

    path("robots.txt", TemplateView.as_view(template_name="robots.txt", content_type="text/plain"),),
]

