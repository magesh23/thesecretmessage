from django.db import models


class UserData(models.Model):
    userid = models.CharField(max_length=20, primary_key=True)
    username = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.IntegerField()
    
    class Meta:
        db_table = "user_data"

class messageData(models.Model):
    user = models.ForeignKey(UserData, related_name='msg_data', on_delete=models.CASCADE)
    message = models.CharField(max_length=1000, blank=True, null=True)
    created_at = models.IntegerField()

    class Meta:
        db_table = "message_data"

