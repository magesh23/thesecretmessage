import string, random 
from ipware import get_client_ip
import requests
from django.conf import settings


def getRandomValue(hash_length):
    s = string.ascii_uppercase + string.ascii_lowercase + string.digits
    # generate a random string of length N
    random_key = ''.join(random.choices(s, k=hash_length))
    return random_key


# google v3 recaptcha validation
def google_recaptcha_validation(request, reCAPTCHA_token):
    # security check for incoming ips
    ipaddress, is_routable = get_client_ip(request)
    # print("ipaddress:",ipaddress)
    # google recaptcha v3 validation

    verify_url = "https://www.google.com/recaptcha/api/siteverify"
    data = {
        "secret": settings.RECAPTCHA_SECRET_KEY,
        "response": reCAPTCHA_token,
        "remoteip": ipaddress
    }
    response = requests.post(url=verify_url, data=data)
    if response.status_code == 200 and reCAPTCHA_token is not None:
        rec = response.json()
        if rec["success"] is False:
            return False
        # valid token
        return True
    else:
        return False
