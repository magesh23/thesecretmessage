from django.contrib.sitemaps import Sitemap
# from django.shortcuts import reverse
from django.urls import reverse


class StaticViewSitemap(Sitemap):
    priority = 1
    changefreq = 'daily'

    def items(self):
        return ["Home","TermsCondition","PrivacyPolicy","Faq","AboutUs","ContactUs"]
    
    def location(self, item):
        return reverse(item)

