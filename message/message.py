from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from message.models import UserData, messageData
from message.utils import google_recaptcha_validation

import time
import logging
# Get an instance of a logger
log = logging.getLogger(__name__)


def createMessage(request):
    try:
        userid = request.POST.get("userid",None)
        message = request.POST.get("message",None)
        reCAPTCHA_token = request.POST.get("token",None)

        # test data
        # userid = "O1Lzamk"
        # message = "test 106"

        if userid is None:
            return JsonResponse({"success":False}, status=403)

        # google v3 recaptcha validation
        if not google_recaptcha_validation(request, reCAPTCHA_token):
            return JsonResponse({"success":False}, status=403)
        
        user = UserData.objects.get(userid = userid)
        data = {
            "user": user, 
            "message": message,
            "created_at": time.time(),
        }
        user = messageData.objects.create(**data)
        
        response =  JsonResponse({"success":True})
        return response
    except Exception as e:
        # print("Error:",e)
        log.error(repr(e))
        return JsonResponse({"success":False}, status=500)



def deleteMessage(request):
    try:
        userid = request.POST.get("userid",None)
        unixtime = request.POST.get("time",None)
        reCAPTCHA_token = request.POST.get("token",None)

        # test data
        # userid = "O1Lzamk"
        # unixtime = 1609510864

        # google v3 recaptcha validation
        if not google_recaptcha_validation(request, reCAPTCHA_token):
            return JsonResponse({"success":False}, status=403)
        
        message_del = messageData.objects.filter(user = userid, created_at = unixtime).delete()
        # print("message_del:",message_del)
        response =  JsonResponse({"success":True})
        return response
    except Exception as e:
        # print("Error:",e)
        log.error(repr(e))
        return JsonResponse({"success":False}, status=500)

def getMessage(request):
    try:
        userid = request.POST.get("userid",None)
        reCAPTCHA_token = request.POST.get("token",None)

        # test data
        userid = "O1Lzamk"

        # google v3 recaptcha validation
        if not google_recaptcha_validation(request, reCAPTCHA_token):
            return JsonResponse({"success":False}, status=403)

        messages = list(messageData.objects.filter(user = userid).values_list("message","created_at").order_by("-created_at"))
        # print("message:",messages)

        return JsonResponse({"success":True, "messages": messages})
    except Exception as e:
        # print("Error:",e)
        log.error(repr(e))
        return JsonResponse({"success":False}, status=500)









