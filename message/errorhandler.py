from django.shortcuts import render


# my_custom_page_not_found_view
def handler404(request, *args, **kwargs):
    return render(request, 'error/404.html', status=404)

# my_custom_error_view
def handler500(request):
    return render(request, 'error/500.html', status=500)

# my_custom_permission_denied_view
def handler403(request, *args, **kwargs):
    return render(request, 'error/404.html', status=403)

# my_custom_bad_request_view
def handler400(request, *args, **kwargs):
    return render(request, 'error/500.html', status=400)

