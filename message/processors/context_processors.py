from django.conf import settings


def get_current_site_url(request):
    return {"CURRENT_SITE_URL": settings.CURRENT_SITE_URL}

