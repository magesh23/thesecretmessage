from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from message.models import UserData, messageData
from message.utils import getRandomValue, google_recaptcha_validation

import time
import logging
# Get an instance of a logger
log = logging.getLogger(__name__)


def createUser(request):
    try:
        username = request.POST.get("name",None)
        reCAPTCHA_token = request.POST.get("token",None)

        # test data
        # username = "Cloe"

        if username is None:
            return JsonResponse({"success":False}, status=403)

        # block if user is already created
        if request.COOKIES.get('user'):
            # print("USER IN")
            # print(request.COOKIES.get('user'))
            return JsonResponse({"success":False}, status=400)
        
        # google v3 recaptcha validation
        if not google_recaptcha_validation(request, reCAPTCHA_token):
            return JsonResponse({"success":False}, status=403)

        userid = getRandomValue(7)
        data = {
            "userid": userid, 
            "username": username,
            "created_at": time.time(),
        }
        user = UserData.objects.create(**data)
        
        response =  JsonResponse({"success":True})
        response.set_cookie('user', userid, max_age=settings.COOKIES_MAX_AGE, secure=settings.COOKIE_SECURE_FLAG)
        return response
    except Exception as e:
        # print("Error:",e)
        log.error(repr(e))
        return JsonResponse({"success":False}, status=500)









