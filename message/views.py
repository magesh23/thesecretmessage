from django.shortcuts import render
from django.http import JsonResponse
from django.shortcuts import redirect
from django.conf import settings
from django.views.decorators.csrf import csrf_protect, ensure_csrf_cookie

from message.models import UserData, messageData

import logging
# Get an instance of a logger
log = logging.getLogger(__name__)


@ensure_csrf_cookie
def home(request):
    try:
        userid = request.COOKIES.get('user',None)
        if userid:
            try:
                user = UserData.objects.get(userid = userid)
                messages = list(messageData.objects.filter(user = userid).values_list("message","created_at").order_by("-created_at"))
                name  = user.username
                return render(request, "home.html", context={
                    "name" : name,
                    "messages" : messages,
                    "userid" : userid,
                    "COPY_LINK" : "{}link/{}".format(settings.CURRENT_SITE_URL,userid),
                    "RECAPTCHA_SITE_KEY" : settings.RECAPTCHA_SITE_KEY,
                }) 
            except Exception as e:
                # userid not in
                # clear the user cookie and redirect to home page
                response = redirect("/")
                response.delete_cookie('user')
                return response
        else:
            # new to the home page
            return render(request, "homenew.html", context={
                "RECAPTCHA_SITE_KEY" : settings.RECAPTCHA_SITE_KEY,
            })
    except Exception as e:
        log.error(repr(e))
        return JsonResponse({"success":False}, status=500)


@ensure_csrf_cookie
def messagePage(request, userid=None):
    try:
        if userid:
            try:
                cookies_userid = request.COOKIES.get('user',None)
                # not allow the owner to message on his wns
                if userid == cookies_userid:
                    return redirect("/")
                user = UserData.objects.get(userid = userid)
                name  = user.username
                return render(request, "message.html", context = {
                    "name" : name,
                    "userid" : userid,
                    "RECAPTCHA_SITE_KEY" : settings.RECAPTCHA_SITE_KEY,
                }) 
            except Exception as e:
                # userid not in
                return JsonResponse({"success":False, "message": "message page"}, status=500)
        else:
            # userid not in
            return redirect("")
    except Exception as e:
        log.error(repr(e))
        return JsonResponse({"success":False}, status=500)




def termsCondition(request):
    return render(request, "meta/terms.html", context = {}) 

def privacyPolicy(request):
    return render(request, "meta/privacy.html", context = {}) 

def faq(request):
    return render(request, "meta/faq.html", context = {}) 

def aboutUs(request):
    return render(request, "meta/aboutus.html", context = {}) 

def contactUs(request):
    return render(request, "meta/contactus.html", context = {}) 









