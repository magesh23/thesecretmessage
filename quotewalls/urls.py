from django.urls import path
from quotewalls import views
from quotewalls import updateddata


urlpatterns = [
    path('api/v1/getdata/', views.getData, name="GetData"),
    path('api/v1/getcategory/', views.getCategoryDat, name="GetCategoryDat"),

    path('file/upload/', updateddata.uploadWalls, name="UploadWalls"),
]
