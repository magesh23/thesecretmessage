from django.apps import AppConfig


class QuotewallsConfig(AppConfig):
    name = 'quotewalls'
