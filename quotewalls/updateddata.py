from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from quotewalls.models import ImageData, CategoryData, CatImgLink
import time
from quotewalls.s3files import uploadS3FileObject


def uploadWalls(request):
    if request.method == 'POST':
        # print("FILES:", len(request.FILES))
        # process post file data        
        for j in request.FILES.getlist("filefield"):
            # upload file to s3
            # s3url = uploadS3FileObject(request.FILES[j], request.FILES[j].name)
            s3url = uploadS3FileObject(j, j.name)
            # print(s3url)
            data = {
                "image_url": s3url,
                "created_at": int(time.time()),
            }
            img = ImageData.objects.create(**data)

        return render(request, "walls_fileupload.html", context={})
    else:
        return render(request, "walls_fileupload.html", context={})

