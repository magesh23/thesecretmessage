from django.db import models


class ImageData(models.Model):
    id = models.AutoField(primary_key=True)
    image_url = models.CharField(max_length=100)
    created_at = models.IntegerField()
    views = models.IntegerField(default=0)
    active = models.BooleanField(default=False)
    premium = models.BooleanField(default=False)

    class Meta:
        db_table = "image_data"


class CategoryData(models.Model):
    id = models.AutoField(primary_key=True)
    category = models.CharField(max_length=100)

    class Meta:
        db_table = "category_data"


class CatImgLink(models.Model):
    image = models.ForeignKey(ImageData, related_name='image_related_data', on_delete=models.CASCADE)
    category = models.ForeignKey(CategoryData, related_name='category_related_data', on_delete=models.CASCADE)

    class Meta:
        db_table = "catimg_link"


