
import string, random 
def getRandomString(k=12):
    s_mix = string.ascii_uppercase + string.ascii_lowercase + string.digits
    # generate a random string of length 12
    return ''.join(random.choices(s_mix, k=k))

import boto3
from django.conf import settings


def uploadS3FileObject(file_object, file_name):
    session = boto3.Session(
        aws_access_key_id= settings.AWS_ACCESS_KEY, 
        aws_secret_access_key= settings.AWS_SECRET_KEY,
        region_name= settings.AWS_REGION_NAME
    )
    s3_resource = session.resource('s3')

    random_string = getRandomString()
    file_path = "mobilewalls/{}.jpg".format(random_string)
    # upload file object to s3 bucket
    bucket = s3_resource.Bucket(settings.AWS_BUCKET_NAME).put_object(
                                                                    Key=file_path, # s3 file path with extension
                                                                    Body=file_object, # readed file object
                                                                    ACL='public-read',
                                                                )
    
    return "https://file-storage-location.s3.amazonaws.com/" + file_path
