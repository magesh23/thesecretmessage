from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.decorators.csrf import csrf_exempt

from quotewalls.models import ImageData, CategoryData, CatImgLink

import logging
# Get an instance of a logger
log = logging.getLogger(__name__)



# list all quotes data
@csrf_exempt
def getData(request):
    try:
        page = request.GET.get('page', 1)
        order = request.GET.get('order', 'desc')

        if order == "desc":
            order = "-created_at"
        elif order == "asc":
            order = "created_at"

        images = ImageData.objects.all().values(
            "image_url","created_at","views","premium").order_by(order)

        paginator = Paginator(images, 10)
        try:
            page_object = paginator.page(page)
        except PageNotAnInteger:
            page_object = paginator.page(1)
        except EmptyPage:
            page_object = paginator.page(paginator.num_pages)
        
        return JsonResponse({
            "success": True,
            "data": list(page_object.object_list),
            "next_page_in": page_object.has_next(),
            "previous_page_in": page_object.has_previous(),
            "no_of_pages": paginator.num_pages,
            })
    except Exception as e:
        log.error(repr(e))
        return JsonResponse({"success":False}, status=500)


# get search category data
@csrf_exempt
def getCategoryDat(request):
    try:
        page = request.GET.get('page', 1)
        category_type = request.GET.get('category')
        order = request.GET.get('order', 'desc')

        # test data
        category_type = "inspiring"

        if order == "desc":
            order = "-created_at"
        elif order == "asc":
            order = "created_at"
        
        items = ImageData.objects.filter(
            image_related_data__category__category = category_type).values(
            "image_url","created_at","views","premium").order_by('created_at')
        

        paginator = Paginator(items, 10)
        try:
            page_object = paginator.page(page)
        except PageNotAnInteger:
            page_object = paginator.page(1)
        except EmptyPage:
            page_object = paginator.page(paginator.num_pages)
        
        return JsonResponse({
            "success": True,
            "data": list(page_object.object_list),
            "next_page_in": page_object.has_next(),
            "previous_page_in": page_object.has_previous(),
            "no_of_pages": paginator.num_pages,
            })
    except Exception as e:
        log.error(repr(e))
        return JsonResponse({"success":False}, status=500)


