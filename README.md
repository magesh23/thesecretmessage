
> thesecretmessage

this project is build on python 3.9\

create virtual environment with
> python3 -m venv venv

install the dependency with this command
> pip install -r requirements.txt


###### create a .env file in the project root directory

```
DEBUG=True
SECRET_KEY=
ALLOWED_HOSTS=127.0.0.1, 192.168.43.135
RECAPTCHA_SITE_KEY=
RECAPTCHA_SECRET_KEY=
CSRF_COOKIE_SECURE=True
CSRF_COOKIE_HTTPONLY=True
CURRENT_SITE=http://127.0.0.1:8000/
```

## ALLOWED_HOSTS
should only allow the domain name

## CURRENT_SITE
to the domain name endpoint

## google recaptcha v3
<ul>
    <li>https://developers.google.com/recaptcha/docs/v3</li>
    <li>https://developers.google.com/recaptcha/docs/verify</li>
</ul>

## Base template file includes
<ul>
    <li>Bootstrap 4</li>
    <li>Fontawesome</li>
    <li>Google fonts</li>
</ul>


## Static files
> python manage.py collectstatic

run this command to move all the static files to staticfiles directory in production
staticfiles - act as a static source



